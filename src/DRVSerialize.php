<?php

namespace Drupal\decoupled_rest_views;


class DRVSerialize extends \Symfony\Component\Serializer\Serializer
{
  
  
    public function drv_serialize($data, $format, array $context = array())
    {
        $data = $this->normalize($data, $format, $context);
        return $this->encode($data, $format, $context);
    }

}