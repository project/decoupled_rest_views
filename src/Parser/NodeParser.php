<?php

namespace Drupal\decoupled_rest_views\Parser;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class NodeParser
{

    protected static $config;

    public function __construct() 
    {
    
    }

    public static function parse($node) 
    {
    
        $config = \Drupal::config('ttv_decoupled_framework.settings')->get('node');
        $node_type = $node['type'][0]['target_id'];
        $fields = $config[$node_type]['fields'];
        foreach ($fields as $field) {
            $value[$field] = $node[$field];
        }
        return $value;
    }

}
