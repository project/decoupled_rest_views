<?php

/**
 * @file
 * Contains \Drupal\decoupled_rest_views\Plugin\views\style\DRVSerializer
 */
namespace Drupal\decoupled_rest_views\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\decoupled_rest_views\DRVSerialize;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "DRV_serializer",
 *   title = @Translation("DRV Serializer"),
 *   help = @Translation("Serializes views row data using the DRV Serializer component."),
 *   display_types = {"data"}
 * )
 */
class DRVSerializer extends Serializer
{
    /**
   * {@inheritdoc}
   */
    public function render() 
    {
        $rows = array();
        $json_output = array();
        // If the Data Entity row plugin is used, this will be an array of entities
        // which will pass through Serializer to one of the registered Normalizers,
        // which will transform it to arrays/scalars. If the Data field row plugin
        // is used, $rows will not contain objects and will pass directly to the
        // Encoder.
        foreach ($this->view->result as $row_index => $row) {
            $this->view->row_index = $row_index;
            $rows[] = $this->view->rowPlugin->render($row);
        }

        unset($this->view->row_index);
        // converting all node objects to array().
        foreach($rows as $node_row) {
            // Converting node obejct to array().
            $json_output[] = !is_array($node_row) && !is_null($node_row) ? $node_row->toArray() : $node_row;
        }
    
        return $this->drv_normalize_json($json_output);
    }
  
    /**
   * This method normalizes the JSON by eliminating all unnecessary fields.
   *
   * @param  type $json
   * @return type
   */
    protected function drv_normalize_json($json) 
    {
        $json = $this->parseFields(drv_normalize_output_array($json));
        return json_encode($json);
    }
  
    /**
   * This method parses all fields with node provided.
   *
   * @param  type $data
   * @return type
   */
    public function parseFields($data) 
    {
        $new_data = $data;
    
        foreach ($data as $value) {
            $new_data = decoupled_rest_views_get_parsed_fields($data);
        }
        return $new_data;
    }
}
